package com.example.twentiethhw.models

data class Article(val title: String?, val imageUrl: String?, val summary: String?, val updatedAt: String?, val events: List<EventsClass>?)
data class EventsClass(val provider: String?)