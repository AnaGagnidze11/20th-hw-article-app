package com.example.twentiethhw.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.example.twentiethhw.R
import com.example.twentiethhw.loadImageWithGlide
import com.example.twentiethhw.models.Article


class ViewPagerAdapter(
    private val context: Context,
) :
    PagerAdapter() {

    private val viewPool = RecyclerView.RecycledViewPool()

    private var items = emptyList<Article>()

    private val listForRecycler = mutableListOf<Map<String, String>>()

    lateinit var layoutInflater: LayoutInflater

    override fun getCount() = items.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.article_layout, container, false)
        val item = items[position]
        val imageUrl = view.findViewById<ImageView>(R.id.imageUrlImgV)
        val title = view.findViewById<TextView>(R.id.titleTxt)
        val updatedAt = view.findViewById<TextView>(R.id.updatedAtTxt)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)

        item.imageUrl?.let { imageUrl.loadImageWithGlide(it) }
        title.text = item.title
        updatedAt.text = item.updatedAt

        val descMap = mutableMapOf<String, String>()
        val providerMap = mutableMapOf<String, String>()

        item.summary?.let { descMap.put("Description", it) }
        item.events?.get(0)?.provider?.let { providerMap.put("Timeline", it) }

        listForRecycler.add(descMap)
        listForRecycler.add(providerMap)

        val infoLayoutManager = LinearLayoutManager(recyclerView.context)

        recyclerView.apply {
            layoutManager = infoLayoutManager
            adapter = RecyclerViewAdapter(listForRecycler)
            setRecycledViewPool(viewPool)
        }

        container.addView(view, 0)


        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun getItems(items: List<Article>){
        this.items = items
        notifyDataSetChanged()
    }
}