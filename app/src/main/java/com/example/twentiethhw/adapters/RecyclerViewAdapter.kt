package com.example.twentiethhw.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.twentiethhw.databinding.InsideCardViewLayoutBinding
import com.example.twentiethhw.models.Article

class RecyclerViewAdapter(private val items: MutableList<Map<String, String>>): RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(InsideCardViewLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount()= items.size

    inner class ItemViewHolder(private val binding: InsideCardViewLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var item: Map<String, String>
        fun bind(){
            item = items[adapterPosition]
            binding.mainText.text = item.keys.toList()[0]
            binding.summaryOrProviderTxt.text = item.values.toList()[0]
        }
    }

}