package com.example.twentiethhw

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.twentiethhw.adapters.ViewPagerAdapter
import com.example.twentiethhw.databinding.FragmentNewsBinding
import com.example.twentiethhw.models.Article
import com.example.twentiethhw.network.ResultControl
import com.example.twentiethhw.viewmodel.ArticleViewModel

class NewsFragment : Fragment() {

    private var _binding: FragmentNewsBinding? = null
    private val binding get() = _binding!!

    private val articleViewModel: ArticleViewModel by viewModels()

    lateinit var adapter: ViewPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null){
            _binding = FragmentNewsBinding.inflate(inflater, container, false)
        }
        init()
        observe()
        return binding.root
    }

    private fun init(){
        adapter = ViewPagerAdapter(requireContext())
        binding.viewPager.adapter = adapter

    }


    private fun observe(){
        articleViewModel._articleLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                ResultControl.Status.SUCCESS -> {
                    adapter.getItems(it.data!!)
                    binding.refresh.isRefreshing = it.loading
                }

                ResultControl.Status.ERROR -> {
                    Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
                    binding.refresh.isRefreshing = it.loading
                }

                ResultControl.Status.LOADING -> {
                    binding.refresh.isRefreshing = it.loading
                }
            }
        })
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}