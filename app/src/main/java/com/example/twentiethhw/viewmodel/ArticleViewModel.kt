package com.example.twentiethhw.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.twentiethhw.models.Article
import com.example.twentiethhw.network.ResultControl
import com.example.twentiethhw.network.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ArticleViewModel: ViewModel() {

    private val articleLiveData = MutableLiveData<ResultControl<List<Article>>>().apply {
        mutableListOf<Article>()
    }
    val _articleLiveData: LiveData<ResultControl<List<Article>>> = articleLiveData

    fun initArticle(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getArticle()
            }
        }
    }

    private suspend fun getArticle(){
        articleLiveData.postValue(ResultControl.loading(true))
        val result = RetrofitService.retrofitService.getArticles()
        if (result.isSuccessful){
            val article = result.body()
            article?.let {
                articleLiveData.postValue(ResultControl.success(it))
            }

        }else{
            articleLiveData.postValue(ResultControl.error(result.message()))
        }
    }
}