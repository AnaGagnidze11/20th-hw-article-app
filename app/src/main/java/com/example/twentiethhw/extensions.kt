package com.example.twentiethhw

import android.widget.ImageView
import com.bumptech.glide.Glide


fun ImageView.loadImageWithGlide(imageUrl: String) {
    Glide.with(this.context).load(imageUrl).placeholder(R.drawable.ic_img_not_found).into(this)
}