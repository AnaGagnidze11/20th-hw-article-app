package com.example.twentiethhw.network

import com.example.twentiethhw.models.Article
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {
    @GET("/v3/articles")
    suspend fun getArticles(): Response<List<Article>>
}